Documents récurrents 
===================


Ce que le projet fait
---------------------
pour imprimer des cartes d'apprentissage (flashcards) pour les commandes de Vim


Pour qui il est fait
--------------------
Ce dépôt est d’abord personnel. Il est cependant ouvert au partage.


Sur quels matériels ou plateformes il tourne
--------------------------------------------
Je travaille sur GNU/Linux, et je me moque bien que cela tourne sur autre chose.

Si vous savez utiliser les outils de base en informatique (ce qui devrait être
votre cas si vous lisez un document d'un dépôt GIT), vous saurez installer les
outils propres à votre OS pour utiliser ce dépôt. Personnellement je ne connais
ni Windows ni MAC OS, et je ne veux pas apprendre à les connaître! C'est pour
moi une question éthique. Je considère qu'il est mal d'utiliser ces produits.


Les dépendances majeures
------------------------
* GIT
	* gitflow
		* http://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html
		* http://www.synbioz.com/blog/git-adopter-un-modele-de-versionnement-efficace
* vim
* LaTeX
* latexmk


Installation
------------
Il n'y a aucune installation. Ce n'est pas un programme, mais juste un ensemble
de documents (qui nécessitent les programmes mentionnés au point précédent).
Il vous suffit donc de cloner le dépôt pour tout avoir sous la main.


Utilisation
-----------
Ce dépôt est géré par GIT. Pour le workflov je suis, scrupuleusement, gitflow.

